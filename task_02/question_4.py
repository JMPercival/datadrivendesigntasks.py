import apache_log_parser
import seaborn
from pandas import DataFrame
from matplotlib import pyplot

#import the various libraries
parser = apache_log_parser.make_parser('%h' '%l' '%u' '%t' '%r' '%>s')
#construct the parser
data = {'category': [],
        'day' : [],
        'time' : []}
#create a dictionary (mapping from key values), this is assigned to the data variable
with open ('basic_web.log') as in_f:
    for line in in_f:
        line = parser(line)

        data['day'].append(line['time_received_tz_datetimeobj'].day)
        data['time'].append(line['time_received_tz_datetimeobj'].hour * 60 + line['time_received_tz_datetimeobj'].minute)

        if 'search' in line['request_url_path']:
            data['category'].append('search')
        elif 'browse' in line['request_url_path']:
            data['category'].append('browse')
        elif 'add-to-cart' in line['request_url_path']:
            data['category'].append('cart')
        elif 'book' in line['request_url_path']:
                data['category'].append('item')
        elif line['request_url_path'] == '/baseline/':
                data['category'].append('baseline')
        elif line['request_url_path'] == '/explore/' or line[request_url_path] == '/review/':
            data['category'].append('modern')


plot = seaborn.stripplot(data = data, x ='category', y ='day')
plot.get_figure().savefig('category_day_stripplot.png')
pyplot.close()
