items = [1,2,3,4] # declaring the array as xs

def maximum(items): #definig the function name as maximum, and the array as the parenthesis

    maximum = items[3] # xs has a maximim index of 3, [0][1][2][3]
    for i in range(len(items)): #for(continuous)loop to find the integer[i] in the range n xs length
        if items[i] > maximum: #if this integer is greater that that above(at start of loop)
            maximum = items[i] #maximum now is that integer (highest number in the loop)
        return maximum      #return, breaks the loop with the desired int
print(maximum(items))          #print the functions results
