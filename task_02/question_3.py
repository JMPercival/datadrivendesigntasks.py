import apache_log_parser
import seaborn
from pandas import DataFrame
from matplotlib import pyplot

#import the various libraries
parser = apache_log_parser.make_parser('%h' '%l' '%u' '%t' '%r' '%>s')
#construct the parser
data = {'day' : [],
        'time' : []}
#create a dictionary (mapping from key values), this is assigned to the data variable
with open ('basic_web.log') as in_f:
    for line in in_f:
        line = parser(line)

        data['day'].append(line['time_received_tz_datetimeobj'].day)
        data['time'].append(line['time_received_tz_datetimeobj'].hour * 60 + line['time_received_tz_datetimeobj'].minute)

data = DataFrame(data)

plot = seaborn.violinplot(data=data, y = 'day')
plot.get_figure().savefig('day_violinplot.png')
pyplot.close()
