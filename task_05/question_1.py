import seaborn

from matplotlib import pyplot
from pandas import read_csv
from scipy import stats

data = read_csv('gardens.csv')

print(data)

plot = seaborn.boxplot(data=data[['gardenA','gardenB','gardenC']])
plot.get_figure().savefig('question_1.png')
pyplot.close()
