On completing question 2, the test results concluded that the data was statistically significantly different. To come to this conclusion
I ran a boxplot and a distplot test, this demonstrated a significant difference between the two areas of differing ozone concentrations. These graphical representations
show that the areas are significantly different. Further analysis, using the levene function(which implements the Levene test for equal variences), recorded
a p-value equalling to: 0.1, which makes it certain that there is a 10% chance we are seeing these variances, due completely random events, such as the ozone
levels are different at each site and not caused by the measuring equipment.

Question_2 Leven Test results: LeveneResult(statistic=2.7509433962264151, pvalue=0.11452047245753871)
