import seaborn

from matplotlib import pyplot
from pandas import read_csv
from scipy import stats

data = read_csv('gardens.csv')

print(data)

plot = seaborn.boxplot(data=data[['gardenB', 'gardenC']])
plot.get_figure().savefig('question4_boxplot.png')
pyplot.close()

# Using the boxplot, looks as though the ozone distributions ae significally different

plot = seaborn.distplot(data['gardenB'])
plot.get_figure().savefig('garden_4B.png')
pyplot.close()

plot = seaborn.distplot(data['gardenC'])
plot.get_figure().savefig('garden_4C.png')
pyplot.close()

# Both distplot graphs are exactly the same, this means that the data is normally distributed

print(stats.levene(data['gardenB'], data['gardenC']))
# 'stats' is imported from scipt library, which provides a vast array of scientific analysis,
# the 'stats' module gives acces to the statistics-speific functionalityself.
# 'Levene' is a test that tests equal variances. (Read the Tutorial that covers this)

print(stats.ttest_ind(data['gardenB'], data['gardenC']))

print(stats.ttest_ind(data['gardenB'], data['gardenC'], equal_var = False))
