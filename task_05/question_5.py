import seaborn
import numpy

from matplotlib import pyplot
from pandas import read_csv
from scipy import stats

data = numpy.array([[24, 120, 65, 212],
                    [66, 94, 76, 32]])

result = stats.chi2_contingency(data)

print('Chi-square', result[0])
print('p-value'), result[1]
