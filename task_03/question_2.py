f = open('prime.txt', 'a')

for n in range(1,101):
    prime = True
    if n < 2:
        prime = False
    else:
        for i in range(2,n):
            if n % i == 0:
                prime = False
        if prime:
            f.write("%s\n" % (n))
f.close()
