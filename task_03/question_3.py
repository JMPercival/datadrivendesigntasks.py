import apache_log_parser
import seaborn
import Levenshtein
from pandas import DataFrame
from matplotlib import pyplot

parser = apache_log_parser.make_parser('%h %l %u %t "%r" %>s')

data = {'ip': [],
        'query': [],
        'query_len': [],
        'reformulation': []}

with open('basic_web.log') as in_f:
    for line in in_f:
        line = parser(line)
        queries = line['request_url_query_dict']
        if 'q' in queries:
            query = queries['q']
            query = query[0]
            query = query.lower()
            data['reformulation'].append('baseline')
            data['ip'].append(line['remote_host'])
            data['query'].append(query)
            data['query_len'].append(len(query.split()))

data = DataFrame(data)

plot = seaborn.countplot(data=data, x='reformulation')
plot.get_figure().savefig('baseline_reformulation.png')
pyplot.close()
