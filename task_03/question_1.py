file = open("welcome.txt", "w+")
# variable = file, this creates 2 args(opens)a new txt file,
# Welcome and (w+) writes
file.write("Hello World!")
# the variable, file(now welcome.txt), writes the parenthesis
print(file)
# prints "Hello World!" in the new file
