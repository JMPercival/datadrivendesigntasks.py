import re

money = re.compile('[0-9]+(\.[0-9]{1,2})?')

print(re.search(money, 'Price: 23.43'))
print(re.search(money, 'Price: 12'))
