import re

telnr = re.compile('((00[1-9][0-9]\s[1-9][0-9]*)|(0[1-9][0-9]*))?\s?[1-9][0-9]+')

print(re.search(telnr, '0044 8291 648392'))
print(re.search(telnr, '148093'))
print(re.search(telnr, '08372 35627'))
