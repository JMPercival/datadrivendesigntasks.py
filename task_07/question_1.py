import re

dates = re.compile(r'(\d\d[/])+(\w+){4}')

####    STEPS   ###
# () = function is braced throughout as this enables repetition of the code inside
# \d = matches digit
# [/] = \ inside the index is used for seperation
# + = used to add elements
# \w+ = short hand for (any letters (a-zA-Z) or digit ([0-9])).
####    END OF STEPS    ###
print(re.search(dates,'12/03/2013'))
print(re.search(dates, '30/02/1999'))
