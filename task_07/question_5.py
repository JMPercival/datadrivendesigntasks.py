import re

# Solution 1

convert = re.compile('[aeo]')


def translate(match):
    if match.group(0) == 'a':
        return '4'
    elif match.group(0) == 'e':
        return '3'
    elif match.group(0) == 'o':
        return '0'
    return ''


print(re.sub(convert, translate, 'Leet Speak'))


# Solution 2

mappings = {'a': '4', 'e': '3', 'o': '0'}
convert = re.compile('[%s]' % ''.join(mappings.keys()))


def translate(match):
    return mappings[match.group(0)]


print(re.sub(convert, translate, 'Leet Speak'))
