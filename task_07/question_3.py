import re

domains = re.compile('([a-zA-Z][a-zA-Z0-9\-]*\.)?[a-zA-Z][a-zA-Z0-9\-]*\.[a-zA-Z][a-zA-Z0-9\-]{1,}')

print(re.search(domains, 'google.com'))
print(re.search(domains, 'www.google.com'))
print(re.search(domains, 'www-2.google.com'))
