from copy import deepcopy
from csv import DictReader,  DictWriter
from datetime import datetime
from collections import Counter
from numpy import array

with open('cleaned_log.csv') as in_f:       # opens the cleaned.log csv file
    with open('sessions.csv', 'w') as out_f:     # sessions file created that contains the session data
        reader = DictReader(in_f)       # Dict reader created this reads each line of the CSV as a dictionary

        fieldnames = deepcopy(reader.fieldnames)     #reads the fieldnames from the reader(by accessing reader.fieldnams)
        fieldnames.append('session')        #fieldnames are read and appended to 'session' field

        writer = DictWriter(out_f, fieldnames=fieldnames)  # used to define the feildnames used by the Dictwriter
        writer.writeheader()

## The sessions variable will hold the last access stamp and session identifier for each "remote_host"

        sessions = {}       #This new variable holds the last access timestamp and the session identifier for each "remote_host"
        for line in reader:     #in the for loop the timestamp is loaded for each request
            if 'Feedfetcher' in line['request_header_user_agent'] or 'AddThis' in line['request_header_user_agent'] or 'Springshare' in line['request_header_user_agent'] or 'feedzirra' in line['request_header_user_agent'] or 'expo9' in line['request_header_user_agent']:
                continue
            timestamp = datetime.strptime(line['time_received_isoformat'], '%Y-%m-%dT%H:%M:%S' )

            if line['remote_host'] in sessions:
                delta = timestamp - sessions[line['remote_host']][0]
                if delta.days > 0 or delta.seconds > 60 < 121:
                    sessions[line['remote_host']] = (timestamp, sessions[line['remote_host']][1] + 1)
                else:
                    sessions[line['remote_host']] = (timestamp, sessions[line['remote_host']][1])
            else:
                sessions[line['remote_host']] = (timestamp, 1)
            line['session'] = sessions[line['remote_host']][1]
            writer.writerow(line)

        with open ('sessions.csv') as in_f: #sessions.csv is opened and fed into DictReader
            reader = DictReader(in_f)
            hits = {}
            session_lengths = {}
            for line in reader:
                identifier = '%s_%s' % (line['remote_host'], line['session'])
                timestamp = datetime.strptime(line['time_received_isoformat'], '%Y-%m-%dT%H:%M:%S')
                if identifier in hits:
                    hits[identifier] = hits[identifier] + 1
                else:
                    hits[identifier] = 1
                if identifier in session_lengths:
                    session_lengths[identifier] = (session_lengths[identifier][0], timestamp)
                else:
                    session_lengths[identifier] = (timestamp, timestamp)
            print(Counter(hits.values()))

            print(Counter(hits.values()))
            for key, (start, end) in session_lengths.items():
                if (end - start).seconds >60 < 120:
                    print(key)
