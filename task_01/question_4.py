import apache_log_parser
from collections import Counter

parser = apache_log_parser.make_parser('%h %l %u %t "%r" %>s')

pages = []
with open ('basic_web.log') as in_f:
    for line in in_f:
        line = parser(line)
        pages.append(line['request_url_path'])

counts = Counter(pages)

selected_pages = [pair[0] for pair in counts.most_common(10)]#most common="baseline"
print(selected_pages)
