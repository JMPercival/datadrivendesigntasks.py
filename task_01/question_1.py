items = [4, 72, 15, 32, 69, 13]

for idx in items :
    if (idx%3 ==0 or idx%5 ==0):
        print idx

# for loop iterates the indexes in the items array.
# the if statement is run, stating if the loop finds numbers in items that are
# multiples of 3 or 5 in list, they will display (print) into the
# console.
