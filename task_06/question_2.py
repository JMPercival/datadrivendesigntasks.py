# import itertools
# #itertools library imported to find the permutations of the array
#
# data = ['A', 'B', 'C', 'D',
#         1, 2, 3, 4, 5, 6]
#
# print list(itertools. permutations(data, 2))
#
# #print method calls the permutation method, from the library, to be evoked onto
# # the data input, into seperate lists containing 2 values.
#
# # NOTE- PERMUTATION= order DOES matter(ACCURATE), COMBINTION= order DOESNT matter(PRESUMPTION)
import itertools


List1 = ['A', 'B', 'C', 'D']
List2 = [1,2,3,4,5,6]

for L in range(0, len(List1)+1):
    for subset in itertools.combinations(List1, L):
        print(subset)

for L in range(0, len(List2)+1):
    for subset in itertools.permutations(List2, L):
        print(subset)
