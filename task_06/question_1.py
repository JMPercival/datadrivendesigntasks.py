from collections import Counter
#not required but I used it cuz I felt like, n what u laptop fucker uperer??

s = ("I have six locks on my door all in a row./"
     "When I go out, I lock every other one./"
     "I figure no matter how long somebody stands there picking the locks,/"
     "they are always locking three.")
     #String is formatted this way to meet PEP8 standards, a string >79 chars flags


def vowels_count(s):
    # definition of function starts
    count = 0
    # counter starts
    vowels = 'aeiouAEIOU'
    # Both standard and capital letter vowels are placed in a variable
    for chars in s:
    #for loop is used so the function can break when complete
        if chars in vowels:
            count += 1
            # each chars checked it meets the variable condition, count adds 1 everytime
    return count
    # when the loop finishes, this produces the count total(sum)


print vowels_count(s)
# the functiion is called by the print method
