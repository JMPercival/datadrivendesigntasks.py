import math
import numpy
import seaborn
from matplotlib import pyplot


sample_a = [numpy.random.randint(10, 25) for _ in range(6)]

print(sample_a)



def stddev(sample) :
    mn = mean(sample)
    var_samples = [math.pow(value - mn, 2) for value in sample]
    var = mean(var_samples)
    return math.sqrt(var)

    print(numpy.std(sample_a))


def percentile(sample, perc):
    sample.sort()
    idx = (len(sample) - 1) * perc / 100.0
    if int(idx) == idx:
        return sample[int(idx)]
    else:
        return (sample[math.floor(idx)] + sample[math.ceil(idx)]) / 2.0


    print(percentile(sample_a, 25), percentile(sample_a, 50), percentile(sample_a, 75))
